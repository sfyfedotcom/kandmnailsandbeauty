<?php
// header('Location: https://app.shedul.com/online_bookings/k-m-nails-beauty-t8ik9fsd/link');
?>
<html>
  <header>
    <style>
      body {
        background-color: #676566;
        color: #fff;
        font: 16px sans-serif;
        margin-top: 4rem;
        text-align: center;
      }
    </style>
  </header>
  <body>
    <p>Dear all,</p>
    <p>Following government advice, we need to take the necessary measures to ensure the safety of our lovely clients and ourselves.</p>
    <p>This is a really uncertain tiime and we have been deliberating the best course of action.</p>
    <p>Therefore, unfortunately we have decided to close K&M Nails and Beauty effective from 9.00am on Monday 23rd March 2020, until further notice. We apologise for the cancellation of appointments and the inconvenience, as soon as we are able to re-open we will notify all so you can re-book through our online booking system.</p>
    <p>Katty and Merlin hope to welcome you all back in the near future.</p>
    <p>STAY SAFE AND LOOK AFTER EACH OTHER</p>
    <p>Thank you for all your support ❤️</p>
  </body>
</html>