<?php

function fetchData ($url) {
  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, $url);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($ch, CURLOPT_TIMEOUT, 20);
  $result = curl_exec($ch);
  curl_close($ch); 
  return json_decode($result);
}

/*
	1. You need to have a Facebook account linked to your Instagram account
	2. Go to developer.facebook.com/apps and login with the Facebook account
	3. Create a "For everything else" app
	4. Enter a name to describe the app
	5. Go to Settings > Basic
	6. Select add platform
	7. Select website
	8. Enter the site url
	9. Save Changes
	10. Go to dashboard
	11. Select Set Up under Instagram, under Add a Product
	12. Select Basic Display on the left
	13. Select Create New App and then Create App
	14. Under "User Token Generator" select "Add or Remove Instagram Testers"
	15. Select "Add Instagram Testers"
	16. Enter the Instagram account's username
	17. Go to Settings > Apps and Websites > Tester Invites in your Instagram account
	18. Accept the invite from the Facebook app
	19. Back in the Facebook app, go to Products > Instagram > Basic Display
	20. Under User Token Generator, select Generate Token next to the tester you just added
	21. Login with the Instagram account and authorize access
	22. Copy the token
*/

$result = fetchData("https://graph.instagram.com/me/media?fields=caption,media_url,permalink&access_token={{instagram}}");

$json = array();

if ($result->data) {
	foreach ($result->data as $entry) {
		$json[] = array(
			"image" => $entry->media_url,
			"text" => $entry->caption,
			"url" => $entry->permalink
		);
	}
}

header('Content-type: application/json');
echo json_encode($json);
