var latLng = [57.145159, -2.106412]
var map = L.map('map', { center: latLng, zoom: 16 })
L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png').addTo(map)
L.marker(latLng).addTo(map)

$(document).ready(function () {      
  if (navigator.userAgent.toLowerCase().indexOf('safari') > -1) {
    $('#directions').attr('href', $('#directions').attr('href').replace('google', 'apple'))  
  }
  
  $.get('/instagram.php', function (data) {
    for (var i = 0; i < data.length; i++) {
      var image = data[i]
      $('#instagram').append([
        '<a aria-label="' + image.text + '" alt="' + image.text + '" href="' + image.url + '" target="_blank" rel="noopener noreferrer">',
        '<div style="background-image: url(' + image.image + ')"></div>',
        '</a>'
      ].join(''))
    }
  })
})
