# Setup

Install [Node](https://nodejs.org).  
Install [Docker](https://docs.docker.com/install/).  
Install [Docker Compose](https://docs.docker.com/compose/install/).

```
git clone git@gitlab.com:sfyfedotcom/kandmnailsandbeauty.git
cd kandmnailsandbeauty
npm i
cp .env.sample .env
```

# Run

```
npm start
```

Should then be available at [http://localhost](http://localhost).

# Clean Up

```
npm run clean
```

# Deployment

```
npm run deploy
```

This will build the html and ftp it to the server defined in `.env`.
