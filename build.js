require('dotenv').config()
const del = require('del')
const fs = require('fs')
const { ncp } = require('ncp')
const minify = require('minify')
const nodeSass = require('node-sass')

const copyDir = (from, to) => new Promise((resolve, reject) => {
  ncp(from, to, (err) => {
    if (err) {
      return reject(err)
    }
    return resolve()
  })
})

const renderCSS = file => new Promise((resolve, reject) => {
  nodeSass.render({ file, outputStyle: 'compressed' }, (err, { css }) => {
    if (err) {
      reject(err)
    }
    return resolve(css.toString('utf8'))
  })
})

const build = async () => {
  let html = fs.readFileSync('./src/index.html', 'utf8')

  const fontAwesome = fs.readFileSync('./node_modules/font-awesome/css/font-awesome.min.css', 'utf8')
  const leafletCSS = fs.readFileSync('./node_modules/leaflet/dist/leaflet.css', 'utf8')
  let css = await renderCSS('./src/style.scss')
  css = [
    fontAwesome.replace(/\.\.\/fonts/g, './fonts'),
    leafletCSS.replace(/images\//g, 'fa-images/'),
    css
  ].join('')

  const javascript = await minify('./src/scripts.js')
  const jquery = fs.readFileSync('./node_modules/jquery/dist/jquery.min.js', 'utf8')
  const leafletJS = fs.readFileSync('./node_modules/leaflet/dist/leaflet.js', 'utf8')

  html = html.replace('{{styles}}', css)
  html = html.replace('{{javascript}}', jquery + leafletJS + javascript)
  html = await minify.html(html)

  del.sync(['./dist'])
  fs.mkdirSync('./dist')
  
  let instagram = fs.readFileSync('./src/instagram.php', 'utf8')
  instagram = instagram.replace('{{instagram}}', process.env.INSTAGRAM)

  const directories = [
    ['./src/book', './dist/book'],
    ['./node_modules/font-awesome/fonts', './dist/fonts'],
    ['./node_modules/leaflet/dist/images', './dist/fa-images']
  ]
  await Promise.all(directories.map(d => copyDir(d[0], d[1])))

  fs.writeFileSync('./dist/index.html', html)
  fs.writeFileSync('./dist/instagram.php', instagram)
  const files = [
    './src/favicon.ico',
    './src/robots.txt',
    './src/logo.png',
    './src/background.jpg'
  ]
  files.forEach(f => {
    fs.copyFileSync(f, f.replace('src', 'dist'))
  })

  console.log('Build complete to /dist')
}

build()
