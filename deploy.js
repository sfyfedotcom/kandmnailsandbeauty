require('dotenv').config()
const Client = require('ftp')
const fs = require('fs')

const readFiles = path => {
  let files = []
  const content = fs.readdirSync(path)
  content.forEach(f => {
    const thisPath = `${path}/${f}`
    if (fs.lstatSync(thisPath).isDirectory()) {
      files = files.concat(readFiles(thisPath))
      return
    }
    files.push(thisPath)
  })
  return files.map(f => f.replace('./dist/', ''))
}

const ftpClient = new Client()

const uploadFile = (c, from, to, cb) => {
  c.delete(to, err => {
    if (err && !/No such file or directory/.test(err.message)) {
      return cb(err)
    }
    c.put(from, to, cb)
  })
}

const putFiles = (c, files, cb, index = 0) => {
  if (index === files.length) {
    cb()
  }
  const file = files[index]
  const from = `./dist/${file}`
  const to = `public_html/${file}`
  let dir = to.split('/')
  dir.pop()
  dir = dir.join('/')
  c.mkdir(dir, true, err => {
    if (err && !/File exists/.test(err.message)) {
      console.log(dir)
      throw err
    }
    uploadFile(c, from, to, err => {
      if (err) {
        console.log(`${from} > ${to}`)
        throw err
      }
      putFiles(c, files, cb, index + 1)
    })
  })
}

ftpClient.on('ready', () => {
  ftpClient.list((err, list) => {
    if (err) {
      throw err
    }
    const files = readFiles('./dist')
    console.log(`Uploading ${files.length} files...`)
    putFiles(ftpClient, readFiles('./dist'), () => {
      ftpClient.end()
      console.log('Deployment finished')
    })
  })
})

ftpClient.on('error', error => {
  throw error
})

ftpClient.connect({
  host: process.env.FTP_HOST,
  user: process.env.FTP_USER,
  password: process.env.FTP_PASSWORD
})
