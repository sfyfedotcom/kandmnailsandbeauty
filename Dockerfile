FROM php:7.2-apache
RUN echo "ServerName localhost" >> /etc/apache2/apache2.conf
COPY dist/ /var/www/html/
EXPOSE 80
CMD ["/usr/sbin/apache2ctl", "-D", "FOREGROUND"]
